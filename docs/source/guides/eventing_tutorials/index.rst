Eventing Tutorials
==================

These are guides on various useful applications of events.

.. toctree::
    :maxdepth: 2
    :caption: Contents

    A-Simple-Mercenary-Shop
    Death-Quotes
    Conditional-Personal-Skills
    Arena