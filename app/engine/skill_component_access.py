from app.editor.settings.main_settings_controller import MainSettingsController
from functools import lru_cache
from app.utilities.data import Data
from app.data.components import Type
from app.data.skill_components import SkillComponent, SkillTags

@lru_cache(1)
def get_cached_skill_components(proj_name: str):
    from app.engine import skill_components

    from app.engine import custom_component_access
    if custom_component_access.get_components():
        # Necessary for get_skill_components to find the item component subclasses
        # defined here
        import custom_components
    # else:
        # custom_component_access.clean()

    subclasses = SkillComponent.__subclasses__()
    # Sort by tag
    subclasses = sorted(subclasses, key=lambda x: list(SkillTags).index(x.tag) if x.tag in list(SkillTags) else 100)
    return Data(subclasses)

def get_skill_components():
    settings = MainSettingsController()
    return get_cached_skill_components(settings.get_current_project())

def get_skill_tags():
    return list(SkillTags)

def get_component(nid):
    _skill_components = get_skill_components()
    base_class = _skill_components.get(nid)
    if base_class:
        return base_class(base_class.value)
    return None

def restore_component(dat):
    nid, value = dat
    _skill_components = get_skill_components()
    base_class = _skill_components.get(nid)
    if base_class:
        if isinstance(base_class.expose, tuple):
            if base_class.expose[0] == Type.List:
                # Need to make a copy
                # so we don't keep the reference around
                copy = base_class(value.copy())
            elif base_class.expose[0] in (Type.Dict, Type.FloatDict):
                val = [v.copy() for v in value]
                copy = base_class(val)
        else:
            copy = base_class(value)
        return copy
    return None

templates = {}

def get_templates():
    return templates.items()
